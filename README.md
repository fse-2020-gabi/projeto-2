# Projeto 2 - FSE

Nome: Gabriela Medeiros da Silva

Matrícula: 16/0121817

## Instruções de Uso

### Instalações iniciais

Instale o Makefile. Para isso, execute o seguinte comando em seu terminal:

``` 
$ sudo apt-get install make 
```

### Compilando e inicando o programa

O programa possui duas pastas: uma com o nome "Client" e outra de nome "Server"

1. Copie a pasta "Server" e todos os arquivos externos à ela para o seu servidor que rodará o *servidor distribuído*.

2. Faça o mesmo com a pasta "Client" (servidor central)

3. Use o Makefile para executar os comandos de compilação em cada uma das pastas:

```
$ make
```
```
$ make start
```

Você pode alterar as configurações de ip e porta no arquivo ``` constantsSocket.c ``` na raíz do projeto. Esse arquivo deve estar em ambos os servidores.

### Execução

Quando o programa iniciar, você verar 3 tabelas que mostram as informações de cada dispositivo e sensor alé das opções oferecidas.

Para escolher uma dessas opções, navegue pelo menu utilizando as setinhas do seu teclado.

Os valores iniciais dos dispositivos é igual a "N", isso indica que ainda não foi lido seu valor no servidor.
### Logs

A cada execução do programa é criado um arquivo de nome único na pasta "Client" em formato csv. Esse arquivo segue o padrão de linha que indica o código do evento e o horário que foi executado. 

Os códigos estão mapeados na seguinte tabela:

| EVENTO | CÓDIGO DO EVENTO |
| ------ | ---------------- |
| ALERTA DE PRESENCA | "A_PRS" |
| RECUPERA TEMPERATURA | "G_TMP" |
| RECUPERA UMIDADE | "G_UMI" |
| DESLIGA LAMPADA COZINHA | "0_DLC" |
| LIGA LAMPADA COZINHA | "1_LLC" |
| RECUPERA LAMPADA COZINHA | "G_GLC" |
| DESLIGA LAMPADA SALA | "0_DLS" |
| LIGA LAMPADA SALA | "1_LLS" |
| RECUPERA LAMPADA SALA | "G_GLS" |
| DESLIGA LAMPADA QUARTO1 | "0_LQ1" |
| LIGA LAMPADA QUARTO1 | "1_LQ1" |
| RECUPERA LAMPADA QUARTO1 | "G_LQ1" |
| DESLIGA LAMPADA QUARTO2 | "0_LQ2" |
| LIGA LAMPADA QUARTO2 | "1_LQ2" |
| RECUPERA LAMPADA QUARTO2 | "G_LQ2" |
| DESLIGA CONDICIONADO QUARTO1 | "0_CQ1" |
| LIGA CONDICIONADO QUARTO1 | "1_CQ1" |
| RECUPERA CONDICIONADO QUARTO1 | "G_CQ1" |
| DESLIGA CONDICIONADO QUARTO2 | "0_CQ2" |
| LIGA CONDICIONADO QUARTO2 | "1_CQ2" |
| RECUPERA CONDICIONADO QUARTO2 | "G_CQ2" |
| RECUPERA PRESENCA SALA | "G_PRS" |
| RECUPERA PRESENCA COZINHA | "G_PRC" |
| RECUPERA PORTA COZINHA | "G_PC0" |
| RECUPERA JANELA COZINHA | "G_JC0" |
| RECUPERA PORTA SALA | "G_PS0" |
| RECUPERA JANELA SALA | "G_JS0" |
| RECUPERA JANELA QUARTO1 | "G_JQ1" |
| RECUPERA JANELA QUARTO2 | "G_JQ2" |


<!-- # Projeto 2

Rasp Servidor Distribuído: 192.168.0.52
Rasp Servidor Central: 192.168.0.53

Links de Acesso:

1. Servidor Distribuído:

ssh gabrielasilva@3.tcp.ngrok.io -p 23900
scp -P 23900 -r constantsSocket.h csv Server gabrielasilva@3.tcp.ngrok.io:~/projeto2

2. Servidor Central:

ssh gabrielasilva@3.tcp.ngrok.io -p 24635
scp -P 24635 -r constantsSocket.h csv Client gabrielasilva@3.tcp.ngrok.io:~/projeto2
scp -P 24635 gabrielasilva@3.tcp.ngrok.io:~/projeto2/Client/194-82419-projeto2.csv ~/Documents/Gabi/UnB/repos/projeto-2/

3. Dashboard

https://embarcados.ngrok.io/dashboard/d42b1880-1934-11eb-b367-b1e3090cb5c1?publicId=8bda2d10-1933-11eb-b367-b1e3090cb5c1 -->
