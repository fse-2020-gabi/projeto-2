#define LAMPADA_COZINHA 17 // 0
#define LAMPADA_SALA 18 // 1
#define LAMPADA_QUARTO1 27 // 2
#define LAMPADA_QUARTO2 22 // 3
#define CONDICIONADO_QUARTO1 13 // 23
#define CONDICIONADO_QUARTO2 19 // 24

#define PRESENCA_SALA 25 // 6
#define PRESENCA_COZINHA 26 // 25
#define PORTA_COZINHA 5 // 21
#define JANELA_COZINHA 6 //22
#define PORTA_SALA 12 //26
#define JANELA_SALA 16 //27
#define JANELA_QUARTO1 20 //28
#define JANELA_QUARTO2 21 //29

#define OUTPUT_ID 1
#define INPUT_ID 0

void desligarDispositivo(int dispositivo);
void ligarDispositivo(int dispositivo);
int lePino(int dispositivo);
void iniciaGPIO();
void trataInterrupcaoGPIO();
