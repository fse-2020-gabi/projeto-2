#define clear() printf("\033[H\033[J")

void trataInterrupcao(int sinal);
void* threadDadosBme(void* unused);
void* threadSensoresGpio(void* unused);
void atualizaDadosBme(int signal);

float getUmidade();
float getTemperatura();

int getPresencaSala();
int getPresencaCozinha();
int getPortaCozinha();
int getJanelaCozinha();
int getPortaSala();
int getJanelaSala();
int getJanelaQuarto1();
int getJanelaQuarto2();

// void setAlarme(int valor);