#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>

#include "threads.h"
#include "../tcp/tcp.h"
#include "../userInterface/userInterface.h"
#include "../../constantsSocket.h"

char alarme[GPIO_S];

char temperatura[FLOAT_S];
char umidade[FLOAT_S];

char lampadaCozinha[GPIO_S];
char lampadaSala[GPIO_S];
char lampadaQuarto1[GPIO_S];
char lampadaQuarto2[GPIO_S];
char condicionadoQuarto1[GPIO_S];
char condicionadoQuarto2[GPIO_S];

char presencaSala[GPIO_S];
char presencaCozinha[GPIO_S];
char portaCozinha[GPIO_S];
char janelaCozinha[GPIO_S];
char portaSala[GPIO_S];
char janelaSala[GPIO_S];
char janelaQuarto1[GPIO_S];
char janelaQuarto2[GPIO_S];

char unused[2]; 

void atualizaUmiETemp(){
    enviaMensagem(GET_TEMPERATURA, GET_TEMPERATURA_S, FLOAT_S, temperatura);
    atualizaInfo(TEMPERATURA_IDX, temperatura, FLOAT_S);

    enviaMensagem(GET_UMIDADE, GET_UMIDADE_S, FLOAT_S, umidade);
    atualizaInfo(UMIDADE_IDX, umidade, FLOAT_S);
}

void ligaAlarme(){
    system("aplay Client/alarme.mp3");
}

void atualizaSensores(){
    enviaMensagem(GET_LAMPADA_COZINHA, GPIO_MS, GPIO_S, lampadaCozinha);
    atualizaInfo(LAMPADA_COZINHA_IDX, lampadaCozinha, GPIO_S + 1);

    enviaMensagem(GET_LAMPADA_SALA, GPIO_MS, GPIO_S, lampadaSala);
    atualizaInfo(LAMPADA_SALA_IDX, lampadaSala, GPIO_S + 1);

    enviaMensagem(GET_LAMPADA_QUARTO1, GPIO_MS, GPIO_S, lampadaQuarto1);
    atualizaInfo(LAMPADA_QUARTO1_IDX, lampadaQuarto1, GPIO_S + 1);

    enviaMensagem(GET_LAMPADA_QUARTO2, GPIO_MS, GPIO_S, lampadaQuarto2);
    atualizaInfo(LAMPADA_QUARTO2_IDX, lampadaQuarto2, GPIO_S + 1);
    
    enviaMensagem(GET_CONDICIONADO_QUARTO1, GPIO_MS, GPIO_S, condicionadoQuarto1);
    atualizaInfo(AR_CONDICIONADO_QUARTO1_IDX, condicionadoQuarto1, GPIO_S + 1);

    enviaMensagem(GET_CONDICIONADO_QUARTO2, GPIO_MS, GPIO_S, condicionadoQuarto2);
    atualizaInfo(AR_CONDICIONADO_QUARTO2_IDX, condicionadoQuarto2, GPIO_S + 1);

    enviaMensagem(GET_PRESENCA_SALA, GPIO_MS, GPIO_S, presencaSala);
    atualizaInfo(PRESENCA_SALA_IDX, presencaSala, GPIO_S + 1);

    enviaMensagem(GET_PRESENCA_COZINHA, GPIO_MS, GPIO_S, presencaCozinha);
    atualizaInfo(PRESENCA_COZINHA_IDX, presencaCozinha, GPIO_S + 1);

    enviaMensagem(GET_PORTA_COZINHA, GPIO_MS, GPIO_S, portaCozinha);
    atualizaInfo(PORTA_COZINHA_IDX, portaCozinha, GPIO_S + 1);

    enviaMensagem(GET_JANELA_COZINHA, GPIO_MS, GPIO_S, janelaCozinha);
    atualizaInfo(JANELA_COZINHA_IDX, janelaCozinha, GPIO_S + 1);

    enviaMensagem(GET_PORTA_SALA, GPIO_MS, GPIO_S, portaSala);
    atualizaInfo(PORTA_SALA_IDX, portaSala, GPIO_S + 1);

    enviaMensagem(GET_JANELA_SALA, GPIO_MS, GPIO_S, janelaSala);
    atualizaInfo(JANELA_SALA_IDX, janelaSala, GPIO_S + 1);

    enviaMensagem(GET_JANELA_QUARTO1, GPIO_MS, GPIO_S, janelaQuarto1);
    atualizaInfo(JANELA_QUARTO1_IDX, janelaQuarto1, GPIO_S + 1);

    enviaMensagem(GET_JANELA_QUARTO2, GPIO_MS, GPIO_S, janelaQuarto2);
    atualizaInfo(JANELA_QUARTO2_IDX, janelaQuarto2, GPIO_S + 1);


    if((presencaSala[0] == '1' 
        || presencaCozinha[0] == '1' 
        || portaCozinha[0] == '1' 
        || janelaCozinha[0] == '1' 
        || portaSala[0] == '1' 
        || janelaSala[0] == '1'
        || janelaQuarto1[0] == '1'
        || janelaQuarto2[0] == '1')
        && alarme[0] == '1'
      ){
        ligaAlarme();
      }

}

void trataInterrupcao(int signal){
    fechaJanelaInterface();
    exit(0);
}

void atualizaDados(int signal){
    atualizaUmiETemp();
    atualizaSensores();
    alarm(1);
}

void* threadMain(void* unused){
    signal(SIGALRM, atualizaDados);

    alarm(1);
    sleep(1);

    return NULL;
}

void setAlarme(char valor){
    char mensagem[2];
    mensagem[0] = valor;
    mensagem[1] = '\0';
    
    atualizaInfo(ALARME_IDX, mensagem, 2);
}

void setLampadaCozinha(char *valor){
    if(valor[0] == '1'){
        enviaMensagem(LIGAR_LAMPADA_COZINHA, GPIO_MS, ACK_S, unused);
    } else {
        enviaMensagem(DESLIGAR_LAMPADA_COZINHA, GPIO_MS, ACK_S, unused);
    }
}

void setLampadaSala(char *valor){
    if(valor[0] == '1'){
        enviaMensagem(LIGAR_LAMPADA_SALA, GPIO_MS, ACK_S, unused);
    } else {
        enviaMensagem(DESLIGAR_LAMPADA_SALA, GPIO_MS, ACK_S, unused);
    }
}

void setLampadaQuarto1(char *valor){
    if(valor[0] == '1'){
        enviaMensagem(LIGAR_LAMPADA_QUARTO1, GPIO_MS, ACK_S, unused);
    } else {
        enviaMensagem(DESLIGAR_LAMPADA_QUARTO1, GPIO_MS, ACK_S, unused);
    }
}

void setLampadaQuarto2(char *valor){
    if(valor[0] == '1'){
        enviaMensagem(LIGAR_LAMPADA_QUARTO2, GPIO_MS, ACK_S, unused);
    } else {
        enviaMensagem(DESLIGAR_LAMPADA_QUARTO2, GPIO_MS, ACK_S, unused);
    }
}

void setCondicionadoQuarto1(char *valor){
    if(valor[0] == '1'){
        enviaMensagem(LIGAR_CONDICIONADO_QUARTO1, GPIO_MS, ACK_S, unused);
    } else {
        enviaMensagem(DESLIGAR_CONDICIONADO_QUARTO1, GPIO_MS, ACK_S, unused);
    }
}

void setCondicionadoQuarto2(char *valor){
    if(valor[0] == '1'){
        enviaMensagem(LIGAR_CONDICIONADO_QUARTO2, GPIO_MS, ACK_S, unused);
    } else {
        enviaMensagem(DESLIGAR_CONDICIONADO_QUARTO2, GPIO_MS, ACK_S, unused);
    }
}