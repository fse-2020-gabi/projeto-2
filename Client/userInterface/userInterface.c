#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include <unistd.h>

#include "userInterface.h"
#include "../threads/threads.h"

WINDOW *menuwin;
WINDOW *dispositivosWin;
WINDOW *valoresWin;

int highlight = -1;
int width = MAX_LENGTH_OP + 10;

void menu(int width){

  char* choices[OPTIONS_QTD] = { 
    "Ligar alarme",
    "Desligar alarme",
    "Ligar lâmpada Sala",
    "Desligar lâmpada Sala",
    "Ligar lâmpada Cozinha",
    "Desligar lâmpada Cozinha",
    "Ligar lâmpada Quarto 1",
    "Desligar lâmpada Quarto 1",
    "Ligar lâmpada Quarto 2",
    "Desligar lâmpada Quarto 2",
    "Ligar ar condicionado Quarto 1",
    "Desligar ar condicionado Quarto 1",
    "Ligar ar condicionado Quarto 2",
    "Desligar ar condicionado Quarto 2"
  };

  mvwprintw(menuwin, 1, (width - 4)/2, "MENU");

  int key;
  while(1){
    for(int i = 0; i < OPTIONS_QTD; i++){
      if(i == highlight)
        wattron(menuwin, A_REVERSE);
      
      mvwprintw(menuwin, i + 3, 1, choices[i]);
      wattroff(menuwin, A_REVERSE);
    }

    key = wgetch(menuwin);

    switch(key){
      case KEY_UP:
        highlight--;
        if(highlight < 0)
          highlight = 0;
        break;
      case KEY_DOWN:
        highlight++;
        if(highlight >= OPTIONS_QTD)
          highlight = OPTIONS_QTD - 1;
        break;
      default:
        break;
    }
    if(key == 10){
      selecionaOpcaoMenu(highlight);
    }
  }
}

void mostraDispositivos(int width){

  char* dispositivos[DISPOSITIVOS_QTD + SENSORES_QTD] = { 
    "Alarme: ",
    "Umidade: ",
    "Temperatura: ",
    "Lâmpada Sala: ",
    "Lâmpada Cozinha: ",
    "Lâmpada Quarto 1: ",
    "Lâmpada Quarto 2: ",
    "Ar condicionado Quarto 1: ",
    "Ar condicionado Quarto 2: ",
    "Presenca Sala: ",
    "Presenca Cozinha: ",
    "Porta Cozinha: ",
    "Janela Cozinha: ",
    "Porta Sala: ",
    "Janela Sala: ",
    "Janela Quarto1: ",
    "Janela Quarto2: "
  };

  mvwprintw(dispositivosWin, 1, (width - 23)/2, "SENSORES E DISPOSITIVOS");

  for(int i = 0; i < (DISPOSITIVOS_QTD + SENSORES_QTD); i++)
    mvwprintw(dispositivosWin, 2 + i, 1, dispositivos[i]);    

  wrefresh(dispositivosWin);
}

void mostraValores(int width){
  mvwprintw(valoresWin, 1, (width - 7)/2, "VALORES");

  for(int i = 0; i < (DISPOSITIVOS_QTD + SENSORES_QTD); i++){
    if(i == ALARME_IDX){
      mvwprintw(valoresWin, 2 + i, 1, "0");
    } else {
      mvwprintw(valoresWin, 2 + i, 1, "N");
    }
  }
  

  wrefresh(valoresWin);
}

void atualizaInfo (int id, char* valor, int tamanho){
  char text[tamanho];
  memcpy(text, valor, tamanho);
  
  mvwprintw(valoresWin,  id + 2, 1, text); 
  box(valoresWin, 0 , 0);
  wrefresh(valoresWin);
}

void* iniciaInterface(void *unused){
  initscr();
  noecho();
  curs_set(0);

  int xMax, yMax;
  getmaxyx(stdscr, yMax, xMax);

  int initY = (yMax - OPTIONS_QTD)/2;

  menuwin = newwin(OPTIONS_QTD + 6, width, initY, 2);
  dispositivosWin = newwin(DISPOSITIVOS_QTD + SENSORES_QTD + 3, width, initY, width + 5);
  valoresWin = newwin(DISPOSITIVOS_QTD + SENSORES_QTD + 3, width, initY, width * 2 + 7);

  nodelay(dispositivosWin, TRUE);
  keypad(menuwin, true);
  box(menuwin, 0 , 0);
  box(dispositivosWin, 0 , 0);
  box(valoresWin, 0 , 0);

  mvwprintw(stdscr, 1, 2, "Olá! Use as setas do teclado pra escolher uma opção do menu:");

  refresh();

  mostraDispositivos(width);
  mostraValores(width);

  menu(width);

  return 0;
}

void fechaJanelaInterface(){
  clear();
  wprintw(stdscr, "Adeus, até logo!\n\n");
  refresh();
  sleep(2);
  endwin();
}

void escreveFeedback(char* texto){

  move(LINE_FEEDBACK, 0);         
  clrtoeol();  
  mvprintw(LINE_FEEDBACK, 2, "--> ");
  mvprintw(LINE_FEEDBACK, 6, texto);
  refresh();
}

void selecionaOpcaoMenu(int opcao){
  switch(highlight){
    case 0:
      // Ligar alarme
      escreveFeedback("Alarme ligado!");
      setAlarme('1');
      break;
    case 1:
      // Desligar alarme
      escreveFeedback("Alarme desligado!");
      setAlarme('0');
      break;
    case 2:
      // Ligar lâmpada da sala
      setLampadaSala("1");
      break;
    case 3:
      // Desligar Lâmpada sala
      setLampadaSala("0");
      break;
    case 4:
      // Ligar lâmpada cozinha
      setLampadaCozinha("1");
      break;
    case 5:
      // Desligar lâmpada cozinha
      setLampadaCozinha("0");
      break;
    case 6:
      // Ligar lâmpada Quarto 1
      setLampadaQuarto1("1");
      break;
    case 7:
      // Desligar lâmpada Quarto 1
      setLampadaQuarto1("0");
      break;
    case 8:
      // Ligar lâmpada quarto 2
      setLampadaQuarto2("1");
      break;
    case 9:
      // Desligar lâmpada quarto 2
      setLampadaQuarto2("0");
      break;
    case 10:
      // Ligar ar condicionado Quarto 1
      setCondicionadoQuarto1("1");
      break;
    case 11:
      // Desligar ar condicionado Quarto 1
      setCondicionadoQuarto1("0");
      break;
    case 12:
      // Ligar ar condicionado quarto 2
      setCondicionadoQuarto2("1");
      break;
    case 13:
      // Desligar ar condicionado quarto 2
      setCondicionadoQuarto2("0");
      break;

    default:
      escreveFeedback("Erro ao selecionar opção, tente novamente\0");
      break;
  }

}