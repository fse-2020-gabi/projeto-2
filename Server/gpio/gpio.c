#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <bcm2835.h>
#include <unistd.h>

#include "gpio.h"

void configuraPinos(){
  bcm2835_gpio_fsel(LAMPADA_COZINHA, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_fsel(LAMPADA_SALA, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_fsel(LAMPADA_QUARTO1, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_fsel(LAMPADA_QUARTO2, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_fsel(CONDICIONADO_QUARTO1, BCM2835_GPIO_FSEL_OUTP);
  bcm2835_gpio_fsel(CONDICIONADO_QUARTO2, BCM2835_GPIO_FSEL_OUTP);

  bcm2835_gpio_fsel(PRESENCA_SALA, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(PRESENCA_COZINHA, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(PORTA_COZINHA, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(JANELA_COZINHA, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(PORTA_SALA, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(JANELA_SALA, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(JANELA_QUARTO1, BCM2835_GPIO_FSEL_INPT);
  bcm2835_gpio_fsel(JANELA_QUARTO2, BCM2835_GPIO_FSEL_INPT);  
}

void desligarDispositivo(int dispositivo){
  bcm2835_gpio_write(dispositivo, 0);
}

void ligarDispositivo(int dispositivo){
  bcm2835_gpio_write(dispositivo, 1);
}

int lePino(int dispositivo){
  return bcm2835_gpio_lev(dispositivo);
}

void trataInterrupcaoGPIO(){
  bcm2835_close();
  exit(0);
}

void iniciaGPIO(){

  if(!bcm2835_init()){
    exit(1);
  }

  configuraPinos();
}
