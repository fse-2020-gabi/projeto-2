#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdint.h>
#include <pthread.h>
#include <stdlib.h>

#include "threads.h"
#include "../gpio/gpio.h"
#include "../bme280/bme280.h"
#include "../tcp/tcp.h"
#include "../../csv/csv.h"
#include "../../constantsSocket.h"

int alarme = 0;

float temp;
float umid;

int presencaSala;
int presencaCozinha;
int portaCozinha;
int janelaCozinha;
int portaSala;
int janelaSala;
int janelaQuarto1;
int janelaQuarto2;

static pthread_mutex_t mutexLock;

void trataInterrupcao(int sinal){
  clear();
  printf("Adeus, até logo!\n\n");
  pthread_mutex_destroy(&mutexLock);
  trataInterrupcaoGPIO();
  trataInterrupcaoTCP();
  sleep(1);
  exit(0);
}

void atualizaDadosBme(int signal){
    int fd = abreI2C();
    temp = solicitaTemperatura(fd);
    umid = solicitaUmidade(fd);

    alarm(1);
}

void* threadDadosBme(void* unused){
  signal(SIGALRM, atualizaDadosBme);
  alarm(1);  
  sleep(1);
  
  return NULL;
}

void* threadSensoresGpio(void* unused){

    iniciaGPIO();

    while(1){

      pthread_mutex_lock(&mutexLock);
      presencaSala = lePino(PRESENCA_SALA);
      pthread_mutex_unlock(&mutexLock);

      pthread_mutex_lock(&mutexLock);
      presencaCozinha = lePino(PRESENCA_COZINHA);
      pthread_mutex_unlock(&mutexLock);

      pthread_mutex_lock(&mutexLock);
      portaCozinha = lePino(PORTA_COZINHA);
      pthread_mutex_unlock(&mutexLock);

      pthread_mutex_lock(&mutexLock);
      janelaCozinha = lePino(JANELA_COZINHA);
      pthread_mutex_unlock(&mutexLock);

      pthread_mutex_lock(&mutexLock);
      portaSala = lePino(PORTA_SALA);
      pthread_mutex_unlock(&mutexLock);

      pthread_mutex_lock(&mutexLock);
      janelaSala = lePino(JANELA_SALA);
      pthread_mutex_unlock(&mutexLock);

      pthread_mutex_lock(&mutexLock);
      janelaQuarto1 = lePino(JANELA_QUARTO1);
      pthread_mutex_unlock(&mutexLock);

      pthread_mutex_lock(&mutexLock);
      janelaQuarto2 = lePino(JANELA_QUARTO2);
      pthread_mutex_unlock(&mutexLock);

      sleep(1);
    }
}

float getTemperatura(){
  return temp;
}

float getUmidade(){
  return umid;
}

int getPresencaSala(){
  return presencaSala;
}

int getPresencaCozinha(){
  return presencaCozinha;
}

int getPortaCozinha(){
  return portaCozinha;
}

int getJanelaCozinha(){
  return janelaCozinha;
}

int getPortaSala(){
  return portaSala;
}

int getJanelaSala(){
  return janelaSala;
}

int getJanelaQuarto1(){
  return janelaQuarto1;
}

int getJanelaQuarto2(){
  return janelaQuarto2;
}