#define OPTIONS_QTD 14 
#define SENSORES_QTD 10
#define MAX_LENGTH_OP 35
#define DISPOSITIVOS_QTD OPTIONS_QTD/2

#define ALARME_IDX 0
#define UMIDADE_IDX 1
#define TEMPERATURA_IDX 2

#define LAMPADA_SALA_IDX 3
#define LAMPADA_COZINHA_IDX 4
#define LAMPADA_QUARTO1_IDX 5
#define LAMPADA_QUARTO2_IDX 6
#define AR_CONDICIONADO_QUARTO1_IDX 7
#define AR_CONDICIONADO_QUARTO2_IDX 8

#define PRESENCA_SALA_IDX 9
#define PRESENCA_COZINHA_IDX 10
#define PORTA_COZINHA_IDX 11
#define JANELA_COZINHA_IDX 12
#define PORTA_SALA_IDX 13
#define JANELA_SALA_IDX 14
#define JANELA_QUARTO1_IDX 15
#define JANELA_QUARTO2_IDX 16

#define LINE_FEEDBACK 4

void* iniciaInterface(void *unused);
void menu(int width);
void mostraDispositivos(int width);
void mostraValores(int width);
void atualizaInfo (int id, char* valor, int tamanho);
void fechaJanelaInterface();
void escreveFeedback(char * texto);
void selecionaOpcaoMenu(int opcao);