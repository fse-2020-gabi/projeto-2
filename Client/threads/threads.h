void* threadMain(void* unused);
void atualizaDados(int signal);
void trataInterrupcao(int signal);
void atualizaSensores();
void atualizaUmiETemp();

void setAlarme(char valor);
void setLampadaCozinha(char *valor);
void setLampadaSala(char *valor);
void setLampadaQuarto1(char *valor);
void setLampadaQuarto2(char *valor);
void setCondicionadoQuarto1(char *valor);
void setCondicionadoQuarto2(char *valor);