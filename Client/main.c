#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>

#include "threads/threads.h"
#include "../csv/csv.h"
#include "userInterface/userInterface.h"
#include "tcp/tcp.h"

int main() {
  pthread_t threadSensores;
  pthread_create(&threadSensores, NULL, &threadMain, NULL);

  pthread_t threadInterface;
  pthread_create(&threadInterface, NULL, &iniciaInterface, NULL);

  signal(SIGTSTP, trataInterrupcao);
  signal(SIGINT, trataInterrupcao);
  signal(SIGKILL, trataInterrupcao);

  cria_csv();

  while(1){
    sleep(2);
  }
  
	return 0;
}
