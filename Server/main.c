#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <signal.h>

#include "tcp/tcp.h"
#include "threads/threads.h"

int main() {

  pthread_t thread_temp_umid;
  pthread_create(&thread_temp_umid, NULL, &threadDadosBme, NULL);

  pthread_t thread_presenca;
  pthread_create(&thread_presenca, NULL, &threadSensoresGpio, NULL);

  signal(SIGTSTP, trataInterrupcao);
  signal(SIGINT, trataInterrupcao);
  signal(SIGKILL, trataInterrupcao);

  iniciaConexao();

  return 0;
}
