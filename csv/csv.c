#include <stdio.h>
#include <string.h>
#include <time.h>

#include "csv.h"
#include "../constantsSocket.h"

char filename[50];

void adiciona_linha(char* code){

  time_t segundos;
  time(&segundos);
  struct tm *data_hora_atual = localtime(&segundos);

  char codeString[CODE_S];
  memcpy(codeString, code, CODE_S);

  FILE *fp; 
  fp = fopen(filename, "a+");

  fprintf(fp, "%d/%d ", data_hora_atual->tm_mday, data_hora_atual->tm_mon+1);
  fprintf(fp, "%d:%d:%d,", data_hora_atual->tm_hour, data_hora_atual->tm_min, data_hora_atual->tm_sec);
  fprintf(fp, "%s\n", codeString);

  fclose(fp);

}
void cria_csv(){

  time_t segundos;
  time(&segundos);
  struct tm *data_hora_atual = localtime(&segundos);

  sprintf(filename, "%d%d-%d%d%d-projeto2.csv", data_hora_atual->tm_mday, data_hora_atual->tm_mon+1, data_hora_atual->tm_hour, data_hora_atual->tm_min, data_hora_atual->tm_sec);
  printf("Nome do Arquivo: %s\n", filename);
   
  FILE *fp;  
  fp = fopen(filename, "w+");
  
  fprintf(fp, "HORARIO, ID AÇÃO\n");

  fclose(fp);
 
}