#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>

#include "tcp.h"
#include "../gpio/gpio.h"
#include "../bme280/bme280.h"
#include "../threads/threads.h"
#include "../../constantsSocket.h"

int clientSocket;
int serverSocket;

void listennerClient(int socket) {
  char buffer[BUFFER_S];

  if(recv(socket, buffer, BUFFER_S, 0) < 0)
    printf("Erro no recebimento do pacote\n");

  executaRequisicao(buffer, socket);
}

void iniciaConexao() {
  int serverSocket;
  struct sockaddr_in servidorAddr;

  int clientSocket;
  struct sockaddr_in clienteAddr;
  unsigned int clientLength;

  if((serverSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    printf("Falha no socket do servidor\n");
    
  memset(&servidorAddr, 0, sizeof(servidorAddr));
  servidorAddr.sin_family = AF_INET;
  servidorAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servidorAddr.sin_port = htons(SERVER_PORT);

  if(bind(serverSocket, (struct sockaddr *) &servidorAddr, sizeof(servidorAddr)) < 0)
    printf("Falha no Bind\n");

  if(listen(serverSocket, 10) < 0)
    printf("Falha no Listen\n");

  while(1){
    printf("Aguardando requisição do cliente\n");
    clientLength = sizeof(clienteAddr);
    if((clientSocket = accept(serverSocket, 
                              (struct sockaddr *) &clienteAddr, 
                              &clientLength)) < 0)
      printf("Conexão Rejeitada\n");

    listennerClient(clientSocket);
    close(clientSocket);
  }
  close(serverSocket);
}

void trataInterrupcaoTCP(){
  close(clientSocket);
  close(serverSocket);
}

void executaRequisicao(char* buffer, int socket) {

  char req[BUFFER_S];
  memcpy(req, buffer, BUFFER_S);

  if(strcmp(req, GET_TEMPERATURA) == 0){

    float res = getTemperatura();
    char mensagem[FLOAT_S];
    sprintf(mensagem, "%f", res);
    if(send(socket, mensagem, FLOAT_S, 0) != FLOAT_S)
      printf("Erro no envio da reposta\n");

  } else if(strcmp(req, GET_UMIDADE) == 0){

    float res = getUmidade();
    char mensagem[FLOAT_S];
    sprintf(mensagem, "%.2f", res);
    if(send(socket, mensagem, FLOAT_S, 0) != FLOAT_S)
      printf("Erro no envio da reposta\n");
      
  } else if(strcmp(req, GET_LAMPADA_COZINHA) == 0){

    int res = lePino(LAMPADA_COZINHA);
    char mensagem[GPIO_S];
    sprintf(mensagem, "%d", res);
    if(send(socket, mensagem, GPIO_S, 0) != GPIO_S)
      printf("Erro no envio da reposta\n");
      
  } else if(strcmp(req, GET_LAMPADA_SALA) == 0){

    int res = lePino(LAMPADA_SALA);
    char mensagem[GPIO_S];
    sprintf(mensagem, "%d", res);
    if(send(socket, mensagem, GPIO_S, 0) != GPIO_S)
      printf("Erro no envio da reposta\n");
      
  } else if(strcmp(req, GET_LAMPADA_QUARTO1) == 0){

    int res = lePino(LAMPADA_QUARTO1);
    char mensagem[GPIO_S];
    sprintf(mensagem, "%d", res);
    if(send(socket, mensagem, GPIO_S, 0) != GPIO_S)
      printf("Erro no envio da reposta\n");
      
  } else if(strcmp(req, GET_LAMPADA_QUARTO2) == 0){

    int res = lePino(LAMPADA_QUARTO2);
    char mensagem[GPIO_S];
    sprintf(mensagem, "%d", res);
    if(send(socket, mensagem, GPIO_S, 0) != GPIO_S)
      printf("Erro no envio da reposta\n");
      
  } else if(strcmp(req, GET_CONDICIONADO_QUARTO1) == 0){

    int res = lePino(CONDICIONADO_QUARTO1);
    char mensagem[GPIO_S];
    sprintf(mensagem, "%d", res);
    if(send(socket, mensagem, GPIO_S, 0) != GPIO_S)
      printf("Erro no envio da reposta\n");
      
  } else if(strcmp(req, GET_CONDICIONADO_QUARTO2) == 0){

    int res = lePino(CONDICIONADO_QUARTO2);
    char mensagem[GPIO_S];
    sprintf(mensagem, "%d", res);
    if(send(socket, mensagem, GPIO_S, 0) != GPIO_S)
      printf("Erro no envio da reposta\n");

  } else if(strcmp(req, LIGAR_LAMPADA_COZINHA) == 0){

    ligarDispositivo(LAMPADA_COZINHA);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, LIGAR_LAMPADA_SALA) == 0){

    ligarDispositivo(LAMPADA_SALA);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, LIGAR_LAMPADA_QUARTO1) == 0){

    ligarDispositivo(LAMPADA_QUARTO1);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, LIGAR_LAMPADA_QUARTO2) == 0){

    ligarDispositivo(LAMPADA_QUARTO2);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, LIGAR_CONDICIONADO_QUARTO1) == 0){

    ligarDispositivo(CONDICIONADO_QUARTO1);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, LIGAR_CONDICIONADO_QUARTO2) == 0){

    ligarDispositivo(CONDICIONADO_QUARTO2);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");

  } else if(strcmp(req, DESLIGAR_LAMPADA_COZINHA) == 0){

    desligarDispositivo(LAMPADA_COZINHA);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, DESLIGAR_LAMPADA_SALA) == 0){

    desligarDispositivo(LAMPADA_SALA);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, DESLIGAR_LAMPADA_QUARTO1) == 0){

    desligarDispositivo(LAMPADA_QUARTO1);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, DESLIGAR_LAMPADA_QUARTO2) == 0){

    desligarDispositivo(LAMPADA_QUARTO2);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, DESLIGAR_CONDICIONADO_QUARTO1) == 0){

    desligarDispositivo(CONDICIONADO_QUARTO1);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");
      
  } else if(strcmp(req, DESLIGAR_CONDICIONADO_QUARTO2) == 0){

    desligarDispositivo(CONDICIONADO_QUARTO2);
    char mensagem[ACK_S] = {'1'};
    if(send(socket, mensagem, ACK_S, 0) != ACK_S)
      printf("Erro no envio do ACK\n");

  } else if(strcmp(req, GET_PRESENCA_SALA) == 0){

    int res = getPresencaSala();
    int size = sizeof(res)/sizeof(int);
    char mensagem[size];

    sprintf(mensagem, "%d", res);

    if(send(socket, mensagem, size, 0) != size)
      printf("Erro no envio da resposta\n");

  } else if(strcmp(req, GET_PRESENCA_COZINHA) == 0){
    
    int res = getPresencaCozinha();
    int size = sizeof(res)/sizeof(int);
    char mensagem[size];

    sprintf(mensagem, "%d", res);

    if(send(socket, mensagem, size, 0) != size)
      printf("Erro no envio da resposta\n");

  } else if(strcmp(req, GET_PORTA_COZINHA) == 0){
    
    int res = getPortaCozinha();
    int size = sizeof(res)/sizeof(int);
    char mensagem[size];

    sprintf(mensagem, "%d", res);

    if(send(socket, mensagem, size, 0) != size)
      printf("Erro no envio da resposta\n");

  } else if(strcmp(req, GET_JANELA_COZINHA) == 0){
    
    int res = getJanelaCozinha();
    int size = sizeof(res)/sizeof(int);
    char mensagem[size];

    sprintf(mensagem, "%d", res);

    if(send(socket, mensagem, size, 0) != size)
      printf("Erro no envio da resposta\n");

  } else if(strcmp(req, GET_PORTA_SALA) == 0){
    
    int res = getPortaSala();
    int size = sizeof(res)/sizeof(int);
    char mensagem[size];

    sprintf(mensagem, "%d", res);

    if(send(socket, mensagem, size, 0) != size)
      printf("Erro no envio da resposta\n");

  } else if(strcmp(req, GET_JANELA_SALA) == 0){
    
    int res = getJanelaSala();
    int size = sizeof(res)/sizeof(int);
    char mensagem[size];

    sprintf(mensagem, "%d", res);

    if(send(socket, mensagem, size, 0) != size)
      printf("Erro no envio da resposta\n");

  } else if(strcmp(req, GET_JANELA_QUARTO1) == 0){
    
    int res = getJanelaQuarto1();
    int size = sizeof(res)/sizeof(int);
    char mensagem[size];

    sprintf(mensagem, "%d", res);

    if(send(socket, mensagem, size, 0) != size)
      printf("Erro no envio da resposta\n");

  } else if(strcmp(req, GET_JANELA_QUARTO2) == 0){
    
    int res = getJanelaQuarto2();
    int size = sizeof(res)/sizeof(int);
    char mensagem[size];

    sprintf(mensagem, "%d", res);

    if(send(socket, mensagem, size, 0) != size)
      printf("Erro no envio da resposta\n");

  }

}