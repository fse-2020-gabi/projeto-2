#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>

#include "tcp.h"
#include "../../csv/csv.h"
#include "../../constantsSocket.h"
#include "../userInterface/userInterface.h"


int clientSocket;

void enviaMensagem(char *mensagem, int tamanhoMensagemEnviada, int tamanhoMensagemRecebida, char* src){
	char response[tamanhoMensagemRecebida + 1];
	int conexao;

  adiciona_linha(mensagem);

	if ((conexao = iniciaConexao()) < 0){
		escreveFeedback("Erro ao estabelecer conexão com o servidor");
		return;
	}

	if (send(clientSocket, mensagem, tamanhoMensagemEnviada, 0) != tamanhoMensagemEnviada){
		escreveFeedback("Erro no envio da requisição");
		return;
	}

	if ((recv(clientSocket, response, tamanhoMensagemRecebida, 0)) < 0){
		escreveFeedback("Erro ao receber resposta do servidor");
		return;
}
	response[tamanhoMensagemRecebida] = '\0';

	memcpy(src, response, tamanhoMensagemRecebida + 1);
	close(clientSocket);
}

int iniciaConexao(){
	struct sockaddr_in servidorAddr;

	// Cria Socket
	if ((clientSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
		return -1;
	}
	// Construir struct sockaddr_in
	memset(&servidorAddr, 0, sizeof(servidorAddr));
	servidorAddr.sin_family = AF_INET;
	servidorAddr.sin_addr.s_addr = inet_addr(SERVER_IP);
	servidorAddr.sin_port = htons(SERVER_PORT);

	if (connect(clientSocket, (struct sockaddr *) &servidorAddr, sizeof(servidorAddr)) < 0){
		return -2;
	}

	return 0;
}
